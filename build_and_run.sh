#!/bin/bash
set -e
pushd DTU-Pay-Libaries/messaging-utilities/
./build.sh
popd

pushd DTU-Pay-Libaries/dtupay-core
./build.sh
popd

pushd token-manager-service
./build.sh
popd

pushd reporting-service-manager
./build.sh
popd

pushd transaction-manager-service
./build.sh
popd

pushd DTU-Pay
./build.sh
popd

./deploy.sh

