#!/bin/bash
set -e
docker container stop $(docker container ls -aq)
docker container rm $(docker container ls -aq)
docker system prune -a
pushd DTU-Pay-Libaries/messaging-utilities/
./build.sh
popd

pushd DTU-Pay-Libaries/dtupay-core
./build.sh
popd

pushd fastmoney-01-bank
mvn clean install
popd
