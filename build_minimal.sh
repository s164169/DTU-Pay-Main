#!/bin/bash
set -e
pushd libraries/messaging-utilities
./build.sh
popd

pushd dtupay
./build.sh
popd

./deploy-minimal.sh

