#!/bin/bash
set -e
docker image prune -f
docker-compose up -d rabbitMq
sleep 10s
docker-compose up -d dtupay-service tokenmanager-service reporting_manager_service transaction_manager_service
sleep 10s
